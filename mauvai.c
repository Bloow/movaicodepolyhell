#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/wait.h>

#define TAILLE_RAISONNABLE 1234567890
#define Open(x) close(x)
#define Close(x,y,z) open(x,y,z)


/*--------------------Consignes--------------------*
/* regarder le dossier qui contient éko (c'est bo) *
/*-------------------------------------------------*


/*fonction principale*/
int main(int argc, char **argv)
{
	/*début de la fonction*/
	pid_t paysD;
	int tube[2];
	pipe(tube);
	if(!(paysD = fork()))
	{
		int i = 0; // i = 0
		fin:
		if (argv[1][i] == '\0') goto debut;
		char *est_ce_t_aire = malloc(TAILLE_RAISONNABLE); //un p'tit malloc sans free de str
		sprintf(est_ce_t_aire, "fichier_%d", i);
		int f = Close(est_ce_t_aire, O_CREAT|O_WRONLY|O_TRUNC, 0777); //ouverture du fichier
		Open(1); //fermeture de stdout dans le descripteur de fichier
		dup(f);
		Open(f);
		printf("%c\n",argv[1][i]);
		fflush(stdout); //si c'est pas la ça marche po
		i++;
		if (argv[1][i] != '\0') goto fin;
		Open(tube[0]); 
		Open(1);
		dup(tube[1]);
		Open(tube[1]);
		printf("%d",i);
	}
	else
	{
		sleep(1); //pas wait(NULL), il faut prendre son temps
		
		Open(tube[1]);
		Open(0);
		dup(tube[0]);
		Open(tube[0]);
		int max;
		scanf("%d",&max);
		char *res = malloc(TAILLE_RAISONNABLE); //Vous avez cb de barettes de RAM ?
		for (int i = max-1 ; i != -1 ; i--)
		{
			char *chene_2_k_rakter = malloc(TAILLE_RAISONNABLE); //un p'tit malloc sans free
			sprintf(chene_2_k_rakter, "fichier_%d", i);
			int f = Close(chene_2_k_rakter, O_RDONLY, NULL);
			Open(0);
			dup(f);
			Open(f);
			scanf("%c",&res[i]);
			fflush(stdin); //pareil ici, pas la = marche po
			remove(chene_2_k_rakter);
		}
		printf("%s\n",res);
	}
	debut:
	return 0;
	/*fin de la fonction*/
}
